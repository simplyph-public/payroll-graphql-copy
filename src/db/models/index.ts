import * as Sequelize from 'sequelize';
import { createContext } from 'dataloader-sequelize';
import path = require('path');
import employeeFactory from './hris/EmployeeModel';
import employeeStatusFactory from './hris/EmployeeStatusModel';
import timeAttendanceFactory from './timekeeping/TimeAttendanceModel';

const config = require(__dirname + '/../config.json')['development'];
const sequelize = new Sequelize(config.url, 
  {
    ...config,
    operatorAliases: Sequelize.Op
  }
);

const Employee = employeeFactory(sequelize);
const EmployeeStatus = employeeStatusFactory(sequelize);
const TimeAttendance = timeAttendanceFactory(sequelize);

Employee.associate = () => {
  Employee.hasMany(TimeAttendance, {
    foreignKey: 'empId'
  })
}

const db = {
  sequelize,
  Sequelize,
  Employee,
  EmployeeStatus,
  TimeAttendance,
}

Object.values(db).forEach((model: any) => {
  if(model.associate) {
      model.associate(db);
  }
});

export default db;