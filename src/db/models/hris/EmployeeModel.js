import * as Sequelize from 'sequelize';

interface Employee {
  id: string;
  empId: number;
  firstName: string;
  lastName: string; 
}

type EmployeeInstance = Sequelize.Instance<Employee> & Employee;

export default (sequelize: Sequelize.Sequelize) => {
  const attributes: SequelizeAttributes<Employee> = {
    id: {
      primaryKey: true,
      type: Sequelize.INTEGER,
      field: 'emp_id'
    },
    empId: {
      type: Sequelize.INTEGER,
      field: 'emp_id'
    },
    firstName: {
      type: Sequelize.STRING,
      field: 'firstname'
    },
    lastName: {
      type: Sequelize.STRING,
      field: 'lastname'
    }
  };

  const options = {
    timestamps: false,
    freezeTableName: true,
  }

  return sequelize.define<EmployeeInstance, Employee>("hris_employee", attributes, options);
}