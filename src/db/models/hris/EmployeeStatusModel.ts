import * as Sequelize from 'sequelize';

interface EmployeeStatus {
  id: string;
  statusId: number;
  desc: string;
  statusType: string; 
}

type EmployeeStatusInstance = Sequelize.Instance<EmployeeStatus> & EmployeeStatus;

export default (sequelize: Sequelize.Sequelize) => {
  const attributes: SequelizeAttributes<EmployeeStatus> = {
    id: {
      primaryKey: true,
      type: Sequelize.INTEGER,
      field: 'status_id'
    },
    statusId: {
      type: Sequelize.INTEGER,
      field: 'status_id'
    },
    desc: {
      type: Sequelize.STRING,
      field: 'status_desc'
    },
    statusType: {
      type: Sequelize.STRING,
      field: 'status_type'
    }
  };

  const options = {
    timestamps: false,
    freezeTableName: true
  }

  return sequelize.define<EmployeeStatusInstance, EmployeeStatus>("hris_employee_status", attributes, options);
}