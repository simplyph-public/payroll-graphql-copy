import * as Sequelize from 'sequelize';

interface TimeAttendance {
  id: string;
  empId: number;
  coveringDay: string;
  day: number;
  hour: number;
  late: number;
  ut: number;
  absent: number;
  ot: number;
  nd: number;
  ob: number;
  leave: number;
}

type TimeAttendanceInstance = Sequelize.Instance<TimeAttendance> & TimeAttendance;

export default (sequelize: Sequelize.Sequelize) => {
  const attributes: SequelizeAttributes<TimeAttendance> = {
    id: {
      primaryKey: true,
      type: Sequelize.INTEGER,
      field: 'autoid'
    },
    empId: {
      type: Sequelize.INTEGER,
      field: 'emp_id'
    },
    coveringDay: {
      type: Sequelize.DATEONLY,
      field: 'firstname'
    },
    day: {
      type: Sequelize.STRING,
      field: 'lastname'
    },
    hour: {
      type: Sequelize.STRING,
      field: 'lastname'
    },
    late: {
      type: Sequelize.STRING,
      field: 'lastname'
    },
    ut: {
      type: Sequelize.STRING,
      field: 'lastname'
    },
    absent: {
      type: Sequelize.STRING,
      field: 'lastname'
    },
    ot: {
      type: Sequelize.STRING,
      field: 'lastname'
    },
    nd: {
      type: Sequelize.STRING,
      field: 'lastname'
    },
    ob: {
      type: Sequelize.STRING,
      field: 'lastname'
    },
    leave: {
      type: Sequelize.STRING,
      field: 'lastname'
    }
  };

  const options = {
    timestamps: false,
    freezeTableName: true
  }

  return sequelize.define<TimeAttendanceInstance, TimeAttendance>("time_attendance2", attributes, options);
}