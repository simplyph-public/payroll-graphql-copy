import { connectionDefinitions } from "graphql-relay";
import { AttendanceSummaryType } from "../AttendanceSummaryType";

export const { connectionType: attendanceSummaryConnection } = connectionDefinitions({ nodeType: AttendanceSummaryType });