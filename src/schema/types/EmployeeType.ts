import { globalIdField, connectionArgs, connectionFromArray } from 'graphql-relay';
import { GraphQLObjectType, GraphQLString, GraphQLInt } from 'graphql';
import { nodeInterface } from '../node';
import { attendanceSummaryConnection } from './connections/AttendanceConnections';

export const EmployeeType = new GraphQLObjectType({
  name: 'Employee',
  description: 'A single user',
  fields: () => ({
    id: globalIdField(),
    empId: {
        type: GraphQLInt,
        resolve: () => 3
    },
    firstName: {
        type: GraphQLString,
        resolve: () => 'knowell'
    },
    lastName: {
        type: GraphQLString,
        resolve: () => 'knowell'
    },
    attendanceSummary: {
      type: attendanceSummaryConnection,
      description: 'List of attendances',
      args: connectionArgs,
      resolve: (summary, args, ctx, inf) => {
        console.log(ctx);
        return connectionFromArray(summary, args)
      }
    },
  }),
  isTypeOf: () => EmployeeType,
  interfaces: () => [nodeInterface],
});