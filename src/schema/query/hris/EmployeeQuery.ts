import { connectionArgs } from 'graphql-relay';
import { GraphQLInt } from 'graphql';
import { EmployeeType } from "../../types/EmployeeType";
import store from '../../../store';
import { attendanceSummaryConnection } from '../../types/connections/AttendanceConnections';

export const EmployeeQuery = {
    type: EmployeeType,
    args: {
        empId: {
            type: GraphQLInt
        }
    },
    resolve: (parentValue, args) => store.timekeeping.summary.get(args.empId)
}