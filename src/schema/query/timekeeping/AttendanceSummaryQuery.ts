import { GraphQLInt, GraphQLList, GraphQLNonNull, GraphQLString, GraphQLBoolean } from 'graphql';
import { AttendanceSummaryType } from './../../types/AttendanceSummaryType';
import store from '../../../store';

export const AttendanceSummaryQuery = {
  type: AttendanceSummaryType,
  args: {
    empIds: {
        type: new GraphQLList(GraphQLInt),
    },
    isActive: {
        type: GraphQLBoolean,
    },
    compIds: {
        type: new GraphQLList(GraphQLInt),
    },
    divIds: {
        type: new GraphQLList(GraphQLInt),
    },
    deptIds: {
        type: new GraphQLList(GraphQLInt),
    },
    desIds: {
        type: new GraphQLList(GraphQLInt),
    },
    funcIds: {
        type: new GraphQLList(GraphQLInt),
    },
    wbIds: {
        type: new GraphQLList(GraphQLInt),
    },
    fromDate: {
        type: new GraphQLNonNull(GraphQLString),
    },
    toDate: {
        type: new GraphQLNonNull(GraphQLString),
    },
  },
  resolve: (a,b) => {
    console.log(a);
    console.log(b);
    return null;
  }
}