import { GraphQLObjectType } from 'graphql';
import { nodeField } from '../node';
import { AttendanceSummaryQuery } from './timekeeping/AttendanceSummaryQuery';
import { EmployeeQuery } from './hris/EmployeeQuery';
import { attendanceSummaryConnection } from '../types/connections/AttendanceConnections';
import { connectionArgs } from 'graphql-relay';


export const queryType = new GraphQLObjectType({
    name: 'Query',
    fields: () => ({
        node: nodeField,
        employee: EmployeeQuery,
    }),
});