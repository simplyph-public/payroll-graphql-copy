import db from "../../db/models";

export class EmployeeDataStore {
  public getEmployeeById(id) {
    return db.Employee.findById(id);
  }
  public getEmployeeWithAttendance(id) {
    return db.Employee.findById(id);
  }
}