import { AttendanceSummaryStore } from './AttendanceSummaryStore';

const timekeeping = {
  summary: new AttendanceSummaryStore()
}

export default timekeeping;