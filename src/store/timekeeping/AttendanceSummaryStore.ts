import db from "../../db/models";
import * as Sequelize from 'sequelize';

const Op = Sequelize.Op;

export class AttendanceSummaryStore {
  public get(id: number) {
    return db.Employee.findAll({
      include: [
        {
          model: db.TimeAttendance,
          attributes: [
            [db.sequelize.fn('SUM', 'day'), 'days']
          ],
          required: true,
        }        
      ],
      where: {
        empId: {
          [Op.eq]: id
        }
      },
      group: 'empId',
    });
  }
}