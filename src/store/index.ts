import hris from './hris';
import timekeeping from './timekeeping';

const store = {
  hris,
  timekeeping
}

export default store;